﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Bililive_VF_Plugin
{
    [Serializable]
    public class VFSetting
    {
        public int TargetNum = 800;
        public int Bias = 30;
    }

    public class SettingUtility
    {
        public VFSetting Setting;

        static string Path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\弹幕姬\Plugins\Data";
        static string FileName = "VirtualFanSetting.json";
        static string FullPath = Path + "\\" + FileName;

        public void Load()
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }
            if (File.Exists(FullPath))
            {
                using (StreamReader reader = new StreamReader(FullPath))
                {
                    JsonSerializer js = new JsonSerializer();
                    JsonTextReader jtr = new JsonTextReader(reader);
                    Setting = js.Deserialize<VFSetting>(jtr);
                }
            }
            else
            {
                Setting = new VFSetting();
            }
        }

        public void Save()
        {
            using (StreamWriter writer = File.CreateText(FullPath))
            {
                JsonSerializer js = new JsonSerializer();
                js.Serialize(writer, Setting);
            }
        }
    }

    public class VirtualFan : BilibiliDM_PluginFramework.DMPlugin
    {
        SettingUtility Setting;

        public VirtualFan()
        {
            this.Connected += VirtualFan_Connected;
            this.Disconnected += VirtualFan_Disconnected;
            this.ReceivedDanmaku += VirtualFan_ReceivedDanmaku;
            this.ReceivedRoomCount += VirtualFan_ReceivedRoomCount;
            this.PluginAuth = "SErAphLi";
            this.PluginName = "虚拟粉丝";
            this.PluginCont = "seraphlivery@gmail.com";
            this.PluginVer = "v0.0.1";
            this.PluginDesc = "增加直播间在线人数";

            Setting = new SettingUtility();
        }


        private void VirtualFan_ReceivedRoomCount(object sender, BilibiliDM_PluginFramework.ReceivedRoomCountArgs e)
        {
            throw new NotImplementedException();
        }

        private void VirtualFan_ReceivedDanmaku(object sender, BilibiliDM_PluginFramework.ReceivedDanmakuArgs e)
        {
            throw new NotImplementedException();
        }

        private void VirtualFan_Disconnected(object sender, BilibiliDM_PluginFramework.DisconnectEvtArgs e)
        {
            throw new NotImplementedException();
        }

        private void VirtualFan_Connected(object sender, BilibiliDM_PluginFramework.ConnectedEvtArgs e)
        {
            throw new NotImplementedException();
        }

        public override void Admin()
        {
            base.Admin();
            Console.WriteLine("Hello World");
            this.Log("Hello World");
            this.AddDM("Hello World", true);
        }

        public override void Stop()
        {
            base.Stop();
            Setting.Save();
        }

        public override void Start()
        {
            base.Start();
            Setting.Load();
        }
    }
}
